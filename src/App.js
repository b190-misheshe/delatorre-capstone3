import './App.css';

import { UserProvider } from "./UserContext";
import { useState , useEffect } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import AppNavbar from './AppNavbar';

import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error"
import Cart from "./components/Cart";
import DashBoard from "./pages/Dashboard";
import ProductView from "./components/ProductView";
import Products from "./pages/Products";

function App() {
  const [ user, setUser ] = useState ({
    id: null,
    isAdmin: null
  });
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect (() => {
    fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(typeof data._id);
      console.log(data._id);
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/products' element={<Products />} />
          <Route path='/products/:productId' element={<ProductView />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/register' element={<Register />} />
          <Route path='/dashboard' element={<DashBoard/>} />
          <Route path='/cart' element={<Cart />} />
          <Route path='*' element={<Error />} />
        </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;