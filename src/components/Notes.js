import { Container, Row } from "react-bootstrap";
import Card from 'react-bootstrap/Card';
import IconShipping from "../images/icon-shipping.png"
import IconCard from "../images/icon-card.png"
import IconAward from "../images/icon-award.png"

export default function Notes () {
   return (
      <Container className="my-5">
         <Row className="my-5">
               <Card md={4} className="mx-4 my-5 notes-card">
                  <Card.Img className="notes-icon mb-1" variant="top" src={IconShipping}/>
                     <Card.Title className="notes-title">Fast Delivery</Card.Title>
                     <Card.Text>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                     </Card.Text>
               </Card>

               <Card md={4} className="mx-4 my-5 notes-card">
                  <Card.Img className="notes-icon mb-1" variant="top" src={IconCard} />
                     <Card.Title className="notes-title">Secured Payment</Card.Title>
                     <Card.Text>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </Card.Text>
               </Card>

               <Card md={4} className="mx-4 my-5 notes-card">
                  <Card.Img className="notes-icon mb-1" variant="top" src={IconAward} />
                     <Card.Title className="notes-title">Quality Product</Card.Title>
                     <Card.Text>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                     </Card.Text>
               </Card>  
         </Row>
      </Container>
   );
}