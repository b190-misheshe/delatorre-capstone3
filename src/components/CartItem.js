import { useState } from 'react';
import { Container, Button, Row, Col, InputGroup, Form, Card } from 'react-bootstrap';

export default function CartItem({productProp}) {
    
    const { name, price, quantity, image}  = productProp

    const [ qty, setQty ] = useState(0);

    const increase = () => {
        setQty(qty => qty + 1);
    };

    const decrease = () => {
        if (qty+quantity > 0) {
            setQty(qty => qty - 1);
        }
    };

    return (
        <Card style={{ width: 350, height: 250 }}>
        <Card.Body >
        <Container>
            <Row>
                <Col xs={12} className='text-center'>
                    <Card.Img src={image} style={{ width: 'auto', height: 100 }}/>
                </Col>
                <Col xs={12} className='text-center' >
                    <Card.Title><strong>{name}</strong></Card.Title>
                    <Card.Text >$ {((qty+quantity)*price).toFixed(2)}</Card.Text>

                    {/* <Counter /> */}
                    <InputGroup className="mb-2 pt-1 px-0 ps-5" style={{width: '15rem'}}>
                        <Button variant='light' className='counter-btn border' onClick={increase}> + </Button>
                            <Form.Control 
                            small
                            className='reg-text text-center counter'
                            value={qty+quantity}
                            onChange={e=> setQty(e.target.value)} 
                            />
                        <Button variant='light' className='counter-btn border' onClick={decrease}> - </Button>
                    </InputGroup>

                </Col>
            </Row>
        </Container>             
        </Card.Body>
        </Card>
    )
}
