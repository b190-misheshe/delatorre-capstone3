import { Row, Col, Card, Container } from 'react-bootstrap';

export default function Highlights(){
	return(
		<Container className="py-5">
			<Row className="mx-3">			
				<Col xs={12} md={6} className="">
					<Card className="cardHighlight1">
						<Card.Body className="p-5">
						<Card.Title className="mb-3">
									<h6 className="text-bold">SKIN CARE TIP: </h6>
								</Card.Title>
							<Card.Text>
								<p>Moisturize your skin, even if you have oily skin.</p>
								<p>For those with oily skin, not hydrating right can mean your skin will produce more oil as compensation.</p>
								<p>The best times to moisturize are right after washing in the morning and right before bed.</p>
								</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={6} className="">
					<Row className="">

						<Card className="cardHighlight2 mb-3">
							<Card.Body className="p-2 pt-4">
								<Card.Title>
									<h6>HEALTHY SKIN IS BEAUTIFUL SKIN</h6>
								</Card.Title>
								<Card.Text>Real skin has texture, pores and even the occasional blemish. The goal is healthy skin, not perfect skin. </Card.Text>
							</Card.Body>
						</Card>
						<Card className="cardHighlight3">
							<Card.Body className="p-2 pt-4">
								<Card.Title>
									<h6>SENSITIVE SKIN TIP</h6>
								</Card.Title>
								<Card.Text>
								<p>It is best to wash your face with lukewarm water, hot water can cause further irritation to sensitive skin.</p>
								</Card.Text>
							</Card.Body>
						</Card>
					</Row>
				</Col>
				<Col xs={12} md={4}>
				</Col>		
			</Row>
		</Container>
	)
}


