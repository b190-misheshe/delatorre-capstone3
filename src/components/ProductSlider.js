import SliderCard from "./SliderCard";
import sliderData from "../data/sliderData";
import Slider from "react-slick";
import { Container } from "react-bootstrap"
import { useEffect, useState } from "react";



export default function ProductSlider () {

   const [ products, setProducts ] = useState([]);

   useEffect(()=> { 
       fetch(`${process.env.REACT_APP_API_URL}/products/all`)
       .then(res=>res.json())
       .then(data=> {
           setProducts(data.map(product=>{
              console.log(products);
               return(
                   <SliderCard key={product._id} productProp={product} />	
               );
           }));
       })
   },[])

   let settings = {
      dot:true,
      infinite: true, 
      speed: 500, 
      slidesToShow: 5,
      slidesToScroll: 1,
      cssEase: "linear",
      initialSlide: 0,
      responsive: [
         {
           breakpoint: 1024,
           settings: {
             slidesToShow: 3,
             slidesToScroll: 3,
             infinite: true,
             dots: true
           }
         },
         {
           breakpoint: 600,
           settings: {
             slidesToShow: 2,
             slidesToScroll: 2,
             initialSlide: 2
           }
         },
         {
           breakpoint: 480,
           settings: {
             slidesToShow: 1,
             slidesToScroll: 1
           }
         }
       ]
   }

   return (
      <Container fluid className="py-5 my-5">
         <h3>New Products</h3>
         <Slider {...settings}>
         {products}
         </Slider>
      </Container>
   )
};
//