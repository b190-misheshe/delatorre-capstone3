import { Link } from 'react-router-dom'
import { Container } from 'react-bootstrap';

export default function ProductCard ({productProp}) {

    const { name, brand, image, price, _id } = productProp;
    console.log(productProp);

    return (
        <Container className="col-lg-3 col-md-4 mb-sm-5 p-3 justify-content-center align-items-center">
            <Container className="bg-image hover-zoom rounded-5 mb-3 justify-content-center" data-mdb-ripple-color="light">
                <Link className="" to={`/products/${_id}`}>
                <img style={{ width: '260px', height: '260px' }}src={image} className="justify-content-center" alt=""/>
                </Link>
            </Container>
            <div className="text-center">
                <p className="text-bold text-colored m-2">{brand}</p>
                <p className="text-normal m-2">{name}</p>
                <p className="text-bold">$ {price}</p>
            </div>
        </Container>
    )
}