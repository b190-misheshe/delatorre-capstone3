import React, { Fragment, useState, useEffect } from 'react'
import ProductCard from "./ProductCard"
import { Link } from 'react-router-dom'

const UserView = ({productData}) => {

   console.log(productData);
   const { name, brand, category, image, price, _id } = productData;

   const [products, setProducts] = useState([]);

   useEffect(() => {
        
      const productsArr = productData.map(product => {
         if(product.isActive === true){
          return (
             <ProductCard productProp={product} key={product._id}/>
          )
         }else{
            return null;
         }
      });

      setProducts(productsArr);

  }, [productData]);
  console.log(productData);

  return(
      // <Fragment>
         <div className="col-md-4 col-lg-2 mb-5 mb-lg-0 p-3">
            <div className="bg-image ripple hover-zoom rounded-5 mb-3" data-mdb-ripple-color="light">
            <Link to={`/products/${productData._id}`}>
            <img style={{ width: 200, height: 200 }}src={productData.image} className="img-feat" alt=""/>
            </Link>
            </div>
            <div className="text-center">
               <h6><a className="feat-text" href="#!">{productData.brand}</a></h6>
               <p><a className="feat-text" href="#!">{productData.name}</a></p>
               <p><span>&#163;</span>{productData.price}</p>
            </div>
         </div>
      // </Fragment>
  );
}

export default UserView

{/* <Fragment>
{products}
</Fragment> */}