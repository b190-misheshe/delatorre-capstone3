import React from 'react'
import { Container, Row, Col } from "react-bootstrap"
import VidContent from '../images/VidContent.mp4'

const Content = () => {
   return (
      <Container className="py-5">
         <Row className="py-5" md={12}>
            <Col md={6}>
               <h5 className="py-4">Something about us</h5>
               <p>
               Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vel semper purus, nec dignissim ligula. Suspendisse at maximus ligula. Pellentesque maximus, nisl sed ornare dapibus, massa dui finibus metus, a aliquam diam odio in turpis. Nam diam eros, fermentum eu nisl nec, semper varius lacus. Integer in eleifend orci.
               </p>
            </Col>
            <Col md={6}>
               <video src={VidContent} autoPlay loop muted className="vid-content px-3"/>
            </Col>
         </Row>
      </Container>
   )
}

export default Content

