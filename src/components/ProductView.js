import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Product () {

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const { productId } = useParams();
	
	const [product, setProduct] =  useState("");
	const [ name, setName ] = useState("");
	const [ quantity, setQuantity ] = useState(1);
	const [ image, setImage ] = useState("");
	const [ brand, setBrand ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price, setPrice ] = useState(0);

	const addToCart = (productId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/addToCart`, {
			method: "POST",
			headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				id: productId,
				quantity: quantity
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if (data === true) {
				Swal.fire({
					title: "Item added to cart",
					icon: "success",
				});
				navigate(`/products/${productId}`);

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(()=>{
		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data=>{
			setProduct(data);
			setName(data.name)
			setBrand(data.brand)
			setDescription(data.description)
			setImage(data.image)
			setPrice(data.price)

		})
	},[productId]);

	console.log(user);
	return(
		<Container className="m-5 pt-5 px-5 ">
			<Row className="mt-5 my-5">
				<Col md={2}>

				</Col>
				<Col md={8}>
					<Card className="p-5">
						<Card.Body className="p-0">
							<Row className="">
							<Col className="">
							<Card.Img src={image} alt="skincare" />
							</Col>
							<Col className="">
							<Card.Title className="text-title text-center mb-3">{name}</Card.Title>
							<Card.Subtitle className="text-bold text-center mb-5">{brand}</Card.Subtitle>
							<Card.Text className="mb-1 text-justify bold">Description:</Card.Text>
							<Card.Text className="mb-4 text-justify">{description}</Card.Text>
							<Card.Text className="text-normal text-bold mb-4">Price: $ {price.toFixed(2)}</Card.Text>
							<Container>
								<Row className="mb-3">
									<Col xs={6}>
										<p className="m-0 py-1">Quantity</p>
									</Col>
									<Col xs={6}>
									<Form.Select value={quantity} onChange={e=>setQuantity(e.target.value)}>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
												<option value="9">9</option>
												<option value="10">10</option>
									</Form.Select>
									</Col>
								</Row>
							</Container>
							{(user.id !== null) ?
							<div className="text-center"> {(user.isAdmin !== true) ?
							<Button className="m-2" variant="primary" onClick={()=>addToCart(productId)} block>Add to Cart</Button>
							:
							<Button className="btn-gray text-center m-2" variant="primary" onClick={()=>addToCart(productId)} block disabled>Add To Cart</Button>
							} </div>
							:
							<Link className='btn btn-danger btn-block' to='/login'>Login to add</Link>
							}
							</Col>
							</Row>
						</Card.Body>
					</Card>
				</Col>
				<Col md={2}>

				</Col>
			</Row>
		</Container>
	)
}

