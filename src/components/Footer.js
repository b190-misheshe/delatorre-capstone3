import React from 'react';
import { MDBFooter, MDBContainer, MDBRow, MDBCol, MDBIcon } from 'mdb-react-ui-kit';
// import { FontAwesomeIcon } from "react-icons/fa";

import { FaFacebook, FaFacebookMessenger, FaTwitter, FaInstagram, FaTiktok } from "react-icons/fa";


export default function Footer () {
  return (
    <MDBFooter className='text-center text-lg-start mt-5' id="footer-bar">

      <section className='mt-5 d-flex justify-content-center justify-content-lg-between p-4 border-bottom'>
        <div className='align-items-center d-none d-lg-block'>
          <span className='text-center'>Get connected with us on social networks:</span>
        </div>
        <div className='me-5'>
          <a className="soc-icons mx-2">
            <FaFacebook />
          </a>
          <a className="soc-icons mx-2">
            <FaFacebookMessenger />
          </a>
        <a className="soc-icons mx-2">
            <FaInstagram />
          </a>
          <a className="soc-icons mx-2">
            <FaTwitter />
          </a>
          <a className="soc-icons mx-2">
            <FaTiktok />
          </a>
        </div>
      </section>

      <section className=''>
        <MDBContainer className='p-4 text-center text-md-start mt-2'>
          <MDBRow>
            
            <MDBCol lg='4' md='12' className='mb-2 mb-md-0'>
              <h6 className='text-uppercase fw-bold mb-4'>
                <MDBIcon icon="gem" className="me-3" />
                Skin's Clear
              </h6>
            </MDBCol>
            <MDBCol lg='4' md='12' className='mb-2 mb-md-0'>
            </MDBCol>
            <MDBCol lg='4' md='12' className='footer-text'>
              <MDBRow className="text-center m">
                <MDBCol md='2' className='mb-2 mb-md-0'>
                  <p>
                    <a href='#!' className='text-reset'>
                      Contact
                    </a>
                  </p>
                </MDBCol>
                <MDBCol md='2' className='mb-2 mb-md-0'>
                  <p>
                    <a href='#!' className='text-reset'>
                      FAQs
                    </a>
                  </p>
                </MDBCol>
                <MDBCol md='2' className='mb-2 mb-md-0'>
                  <p>
                    <a href='#!' className='text-reset'>
                      Order
                    </a>
                  </p>
                </MDBCol>
                <MDBCol md='2' className='mb-2 mb-md-0'>
                  <p>
                    <a href='#!' className='text-reset'>
                      Shipping
                    </a>
                  </p>
                </MDBCol>
                <MDBCol md='2' className='mb-2 mb-md-0'>
                  <p>
                    <a href='#!' className='text-reset'>
                      Returns
                    </a>
                  </p>
                </MDBCol>
                <MDBCol md='2' className='mb-2 mb-md-0'>
                  <p>
                    <a href='#!' className='text-reset'>
                      Privacy
                    </a>
                  </p>
                </MDBCol>
              </MDBRow>
            </MDBCol>

          </MDBRow>
        </MDBContainer>
      </section>

      <div className='text-center p-3 footer-icon'>
        © 2022 Copyright : Skin's Clear.
      </div>
    </MDBFooter>
  );
}
