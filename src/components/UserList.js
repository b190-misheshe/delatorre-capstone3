import React, {useState} from 'react'
import { Button, InputGroup, Form, DropdownButton, Dropdown } from 'react-bootstrap'
import { FaRegEdit } from "react-icons/fa";
import { AdminContext } from "../pages/AdminView";
import Swal from "sweetalert2";

const UserList = ({ userProp }) => {

    const { fullName, email, mobileNo, isAdmin, _id } = userProp;

    const [showEdit, setShowEdit] = useState(false);
    const openEdit = () => setShowEdit(true); 
    const closeEdit = () => setShowEdit(false);

    const setAdmin = () => {
      fetch(`${ process.env.REACT_APP_API_URL }/users/setAdmin`,{
         method: "PUT",
       headers: {
         'Content-Type': 'application/json',
           Authorization: `Bearer ${localStorage.getItem('token')}`
         },
         body: JSON.stringify({
            id: _id,
            isAdmin: isAdmin,
         })
      })
      .then(res => res.json())
      .then(data => {
         if (data === true) {
            Swal.fire({
              title: "User set as admin.",
              icon: "success",
         })
         } else {
            Swal.fire({
               title: "Something went wrong.",
               icon: "error",
            })
         }
      })
    }

    return (
        <>
        <tr key={_id}>
            <td>{fullName}</td>
            <td>{email}</td>
            <td>{mobileNo}</td>
            <td>{_id}</td>
            <td>
            {isAdmin ?
            <p>Admin</p>
            :
            <p>Non-Admin</p>
            }
            </td>
            <td className="justify-content-center">
        

        <DropdownButton
          variant="outline-info"
          title=""
          id="input-group-dropdown-2"
          size="sm" 
        >
        {isAdmin ? 
          <Dropdown.Item onClick={() => setAdmin()}>Set As Non-Admin</Dropdown.Item>
        :
          <Dropdown.Item onClick={() => setAdmin()}>Set As Admin</Dropdown.Item>
        }
          <Dropdown.Divider />
          <Dropdown.Item href="#">View Orders</Dropdown.Item>
        </DropdownButton>
            </td>
        </tr>
        </>
    )
}

export default UserList































// import { useState } from 'react';
// import { Link } from 'react-router-dom'

// import Table from 'react-bootstrap/Table';


// export default function UserList ({userProp}) {

//     const { fullName, email, mobileNo, isAdmin, _id } = userProp;
//     console.log(userProp)

//     return (

//         <Table responsive>
//         <thead className="custom">
//            ...
//         </thead>
//         <tbody>
        
//             <td>{fullName}</td>
//           <td>{email}</td>
//           <td>{mobileNo}</td>
//           <td>{isAdmin}</td>
        
//         </tbody>
//     </Table>

//     )
// }