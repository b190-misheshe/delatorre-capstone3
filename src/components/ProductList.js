import React, { useEffect, useState } from 'react'
import { Button, Modal, Form,  } from "react-bootstrap";
import Swal from 'sweetalert2';
import { FaRegEdit } from "react-icons/fa";
import { RiInboxArchiveLine, RiInboxUnarchiveFill } from "react-icons/ri";

const ProductList = ({ productProp }) => {

	const { _id, name, brand, description, category, quantity, price, isActive } = productProp;

	const [productId, setProductId] = useState(_id);
   const [productName, setProductName] = useState(name);
	const [productBrand, setProductBrand] = useState(brand);
	const [productDescription, setProductDescription] = useState(description);
   const [productCategory, setProductCategory] = useState(category);
	const [productQuantity, setProductQuantity] = useState(quantity);
	const [productPrice, setProductPrice] = useState(price);
	const [productIsActive, setProductIsActive] = useState(true);
	const [productNewArr, setProductNewArr] = useState([]);

   const [showEdit, setShowEdit] = useState(false);
	const openEdit = () => setShowEdit(true); 
	const closeEdit = () => setShowEdit(false);

   const editProduct = (e, _id) => {	
		e.preventDefault();
		fetch(`${ process.env.REACT_APP_API_URL }/products/`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				id: productId,
				name: productName,
				brand: productBrand,
				description: productDescription,
				quantity: productQuantity, 
				price: productPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			closeEdit();
			// window.location.reload();
			if (data === true) {					
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated."
				});			
			} else {			
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		})
	}

	const archiveProduct = () => {
		console.log(isActive);
		fetch(`${ process.env.REACT_APP_API_URL }/products/archive`, {
			 method: "PUT",
		 	 headers: {
			 'Content-Type': 'application/json',
				 Authorization: `Bearer ${localStorage.getItem('token')}`
			 },
			 body: JSON.stringify({
					id: _id,
					isActive: isActive,
			 })
		})
		.then(res => res.json())
		.then(data => {
			 if (data === true) {
				Swal.fire({
				title: "Success",
				icon: "success",
				text: "Product status changed."
				})
			 } else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
						});			
			 }
			})
	}

		return (
			<>
				<tr key={_id}>
					<td>{name}</td>
					<td>{_id}</td>
					<td>{brand}</td>
					<td>{category}</td>
					<td>{quantity}</td>
					<td>{price}</td>
					<td>
						{isActive ? <span>Available</span>
						: <span>Unavailable</span>
						}
					</td>
					<td className="justify-content-center">
						<Button onClick={openEdit} size="sm" className="m-1">
							<FaRegEdit />
						</Button>
						{isActive ?
						<Button variant="danger" size="sm" className="m-1"
							onClick={() => archiveProduct({ _id, isActive })}>
							<RiInboxArchiveLine className="icon-archive"/>
						</Button>
						:
						<Button variant="success" size="sm" className="m-1"
							onClick={() => archiveProduct({ _id, isActive })}>
							<RiInboxUnarchiveFill className="icon-archive" />
						</Button>}
					</td>
				</tr>
				
		<Modal className="mt-5 pt-5" show={showEdit} onHide={closeEdit}>
			<Form onSubmit={e => editProduct(e, _id)}>
				<Modal.Header closeButton>
					<Modal.Title>Edit Product</Modal.Title>
				</Modal.Header>

				<Modal.Body>	
					<Form.Group controlId="productName">
						<Form.Label>Name</Form.Label>
						<Form.Control type="text" value={productName} onChange={e => setProductName(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="productBrand">
						<Form.Label>Brand</Form.Label>
						<Form.Control type="text" value={productBrand}  onChange={e => setProductBrand(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="productDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control type="text" value={productDescription}  onChange={e => setProductDescription(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="productQuantity">
						<Form.Label>Quantity</Form.Label>
						<Form.Control type="number" value={productQuantity}  onChange={e => setProductQuantity(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="productPrice">
						<Form.Label>Price</Form.Label>
						<Form.Control type="number" value={productPrice}  onChange={e => setProductPrice(e.target.value)} required/>
					</Form.Group>
					</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" 
					onClick={closeEdit} >Close</Button>
					<Button variant="success" type="submit">Submit</Button>
				</Modal.Footer>
			</Form>
		</Modal>
		</>
   )
}

export default ProductList