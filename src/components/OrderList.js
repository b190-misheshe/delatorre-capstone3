import React, {useEffect, useState}from 'react'
import { Button, Dropdown, DropdownButton } from 'react-bootstrap'
import OrderItem from "./OrderItem";

const OrderList = ({ orderProp }) => {

   const { email, _id, item, productId, price, quantity, totalAmount, purchasedOn, status} = orderProp;

   const [showEdit, setShowEdit] = useState(false);
  //  const openEdit = () => setShowEdit(true); 
  //  const closeEdit = () => setShowEdit(false);

  const [products, setProducts] = useState("")

  useEffect(()=> { 
    
		fetch(`${ process.env.REACT_APP_API_URL }/users/orderAdmin/${_id}`, {
      method: "GET",
      headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
    })
    .then(res => res.json())
		.then(res=>{
      console.log(res.products)
      const order = res.products
      setProducts(order.map(res => {
        console.log(products)
        return (
          <OrderItem key={res.productId} productProp={res} />
          
        )
      }))
    })

	},[])
 

  return (
   <tr>
      <td>{email}</td>
      <td>{_id}</td>
      <td>{products}</td>
      <td>{totalAmount}</td>
      <td>{purchasedOn}</td>
      <td>{status}</td>
      <td className="justify-content-center">
        <DropdownButton
          variant="outline-info"
          title=""
          id="input-group-dropdown-2"
          size="sm" 
        >
          <Dropdown.Item>Change Status</Dropdown.Item>
          <Dropdown.Divider />
          <Dropdown.Item>Pending</Dropdown.Item>
          <Dropdown.Item>Shipped</Dropdown.Item>
          <Dropdown.Item>Cancelled</Dropdown.Item>
          <Dropdown.Item>Refunded</Dropdown.Item>
          <Dropdown.Item>Completed</Dropdown.Item>
        </DropdownButton>
      </td>
   </tr>
  )
}

export default OrderList