import React from 'react';
import Slider from "react-slick";
import { Card } from 'react-bootstrap'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Link } from "react-router-dom";

const SliderCard = ({productProp}) => {

  const { name, image, _id } = productProp;

  return (

    <Slider>
      <Card className="slider-content text-center px-3 py-5">
        <Link to={`/products/${_id}`}>
          <Card.Img className="slider-img pb-4" src={image} alt="skincare" />
        </Link>
        <Card.Body className="slider-body">
          <Card.Title className="text-normal">{name}</Card.Title>
        </Card.Body> 
        {/* <Card.Body className="slider-body text-justify p-0">
          <Card.Link className="slider-link" href="#">View</Card.Link>
        <Card.Link className="slider-link" href="#">Add to Cart</Card.Link>
        </Card.Body> */}
      </Card>
    </Slider> 
  )
}

export default SliderCard
