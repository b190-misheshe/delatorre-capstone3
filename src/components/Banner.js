import { Container, Row, Col } from 'react-bootstrap';

export default function Banner(){
	return(

		<Container fluid className="bgc-image mt-5 md-ps-5 ">
			<Container className="pt-5 md-ps-3">
				<Row className="pt-5 px-0">
					<Col md={6}>
						<h2 className="text-header pb-1 mt-5">
						<span className="bgc-text-1a">Be good</span>&nbsp; to your skin...</h2>
						<h1 className="bgc-text-2 pb-4">You'll wear it everyday, for the rest of your life.</h1>
						<p className="pb-3">Investing early in the well-being of your skin, accompanying balanced skin care, will not only better defend it from the severe belongings of cold, but further hold you appearing and impression your best throughout the old age.</p>
						<button class="custom-btn btn"><span>Shop Now</span></button>
					</Col>
				</Row>
			</Container>
		</Container>
	)
}
