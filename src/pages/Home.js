import { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Content from "../components/Content";
import Notes from '../components/Notes'
import Footer from '../components/Footer'
import Slide from 'react-reveal/Slide';
import ScrollToTop from "../components/ScrollToTop";
import ProductSlider from "../components/ProductSlider";

export default function Home(){
	return(
		<Fragment>
			<Banner />
			<Slide bottom>
				<ProductSlider />
			</Slide>
			<Slide bottom>
				<Highlights />
			</Slide>
			<Slide bottom>
				<Content />
			</Slide>
			<ScrollToTop />
			<Slide bottom>
				<Notes />
			</Slide>
			<Footer />
		</Fragment>
	)
}