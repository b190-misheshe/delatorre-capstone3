import { Fragment, useEffect, useState, useContext } from 'react';
import UserContext from "../UserContext";
import AdminView from "./AdminView";
import Products from "./Products";
// import UserContext from '../UserContext';

const DashBoard = () => {

   // const { user } = useContext(UserContext);
   const [products, setProducts] = useState([]);

   const fetchData = () => {
      fetch(`${ process.env.REACT_APP_API_URL }/products`)
      .then(res => res.json())
      .then(data => {
         // console.log(data);
         setProducts(data);
      });
   }

   useEffect(() => {
      fetchData();
   }, []);

   return (
      <Fragment>
         <AdminView fetchData={fetchData}/>
         {/* <Products fetchData={fetchData} />  */}
      </Fragment>
  )
}

export default DashBoard