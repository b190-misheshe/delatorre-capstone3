import React, { useEffect, useState, createContext } from 'react'
import { Container, Table, Row, Col, Button, ButtonGroup, ButtonToolbar, Modal, Form } from "react-bootstrap";
import Swal from 'sweetalert2';
import UserList from "../components/UserList";
import ProductList from "../components/ProductList";
import OrderList from "../components/OrderList";

export const AdminContext = createContext()

const AdminView = () => {

   const [productName, setProductName] = useState("");
	const [productBrand, setProductBrand] = useState("");
	const [productImage, setProductImage] = useState("");
	const [productDescription, setProductDescription] = useState("");
   const [productCategory, setProductCategory] = useState("");
	const [productQuantity, setProductQuantity] = useState("");
	const [productPrice, setProductPrice] = useState();

   const [users, setUsers] = useState([]);
   const [active, setActive] = useState("ProductsData");

   const getUsers = () => {
   fetch(`${ process.env.REACT_APP_API_URL }/users`)
   .then(res => res.json())
   .then(data => {   
      console.log(data);
      setUsers(data.map(user => {
         return (
            <UserList key={user._id} userProp={user} />
         )
      }))
   })
   }

   const getProducts = () => {
   fetch(`${ process.env.REACT_APP_API_URL }/products/all`)
   .then(res => res.json())
   .then(data => {
      setUsers(data.map(product => {
         return (
            <ProductList key={product._id} productProp={product} />
         )
      }))
   })
   }

   useEffect(() => {
      getProducts();
   }, []);

   const getOrders = () => {
      fetch(`${ process.env.REACT_APP_API_URL }/users/orders`)
      .then(res => res.json())
      .then(data => {
         setUsers(data.map(order => {
            return (
               <OrderList key={order._id} orderProp={order}/>
            )
         }))
      })
      }
   
   const [showAdd, setShowAdd] = useState(false);
   const openAdd = () => setShowAdd(true);
   const closeAdd = () => setShowAdd(false);
      
   const addProduct = (e) => {
      e.preventDefault()
      fetch(`${ process.env.REACT_APP_API_URL }/products`, {
         method: 'POST',
         headers: {
            Authorization: `Bearer ${ localStorage.getItem('token') }`,
            'Content-Type': 'application/json'
         },
         body: JSON.stringify({
            name: productName,
            image: productImage,
				brand: productBrand,
				description: productDescription,
            category: productCategory,
				price: productPrice,
				quantity: productQuantity 
         })
      }) 
      .then(res => res.json())
      .then(data => {
         if (data === true) {
            Swal.fire({
               title: "Success",
               icon: "success",
               text: "Product successfully added."					
            })          
            setProductName("")
            setProductImage("")
            setProductBrand("")
            setProductDescription("")
            setProductCategory("")
            setProductQuantity(0)
            setProductPrice(0)

            getProducts();
            closeAdd();
         } else {
            Swal.fire({
               title: "Something went wrong",
               icon: "error",
               text: "Please try again."
            })
         }
      })
   }
           
   return (
      <>
      <Container className="mt-5 pt-5 px-3 mb-4">
         <Row>
            <Col className='title text-center'>Admin Dashboard</Col>
         </Row>
         <Row className="square border border-0">
            <ButtonToolbar className="justify-content-between pt-3" 
            aria-label="Toolbar with Button groups">
               <ButtonGroup variant="light" className="p-0" aria-label="First Group" size="sm">
                  <Button className="px-5 py-2" onClick={()=> {getUsers(); setActive("UsersData")} }>Users</Button>
                  <Button className="px-5 py-2" onClick={()=> {getOrders(); setActive("OrdersData")} }>Orders</Button>
                  <Button className="px-5 py-2" onClick={()=> {getProducts(); setActive("ProductsData")} }>Products</Button>
               </ButtonGroup>
               <ButtonGroup className="p-0" aria-label="Basic example" size="sm">
                  <Button className="px-5" onClick={openAdd}>+&#160; Add New Product</Button>
               </ButtonGroup>
            </ButtonToolbar>
         </Row>
      </Container>
      <Container className="table-container mt-2 px-3 mb-5" size="sm">
         {active === "UsersData" &&
         <Table responsive striped bordered hover>
         <thead className="thead bg-info text-white text-center">
            <tr>
               <th>Full Name</th>
               <th>Email</th>
               <th>Mobile No.</th>
               <th>User ID</th>
               <th>Role</th>
               <th>Action</th>
            </tr>
         </thead>
         <tbody>
            {users}                
         </tbody>
         </Table>}  

         {active === "OrdersData" &&
         <Table responsive striped bordered hover size="sm">
            <thead className="thead bg-info text-white text-center">
               <tr>
                  <th>Customer</th>
                  <th>Order ID</th>
                  <th>Item</th>
                  <th>Total Amount</th>
                  <th>Date Purchased</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               {users}                
            </tbody>
         </Table>}   

         {active === "ProductsData" &&
         <Table responsive striped bordered hover size="sm">
            <thead className="thead bg-info text-white text-center">
               <tr>
                  <th>Product</th>
                  <th>Product ID</th>
                  <th>Brand</th>
                  <th>Category</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               {users}                
            </tbody>
         </Table>}    
      </Container>

    {/*ADD MODAL*/}
   <Modal className="mt-5 pt-5" show={showAdd} onHide={closeAdd}>
      <Form onSubmit={e => addProduct(e)}>
         <Modal.Header closeButton>
            <Modal.Title>Add Product</Modal.Title>
         </Modal.Header>
         <Modal.Body>	
            <Form.Group controlId="productName">
               <Form.Label>Name</Form.Label>
               <Form.Control type="text" value={productName}
               onChange={e => setProductName(e.target.value)} required/>
            </Form.Group>
            <Form.Group controlId="productBrand">
               <Form.Label>Image</Form.Label>
               <Form.Control type="text" value={productImage}
               onChange={e => setProductImage(e.target.value)} required/>
            </Form.Group>
            <Form.Group controlId="productBrand">
               <Form.Label>Brand</Form.Label>
               <Form.Control type="text" value={productBrand}
               onChange={e => setProductBrand(e.target.value)} required/>
            </Form.Group>
            <Form.Group controlId="productBrand">
               <Form.Label>Description</Form.Label>
               <Form.Control type="text" value={productDescription}
               onChange={e => setProductDescription(e.target.value)} required/>
            </Form.Group>
            <Form.Group controlId="productBrand">
               <Form.Label>Category</Form.Label>
               <Form.Control type="text" value={productCategory}
               onChange={e => setProductCategory(e.target.value)} required/>
            </Form.Group>
            <Form.Group controlId="productDescription">
               <Form.Label>Quantity</Form.Label>
               <Form.Control type="number" value={productQuantity}
               onChange={e => setProductQuantity(e.target.value)} required/>
            </Form.Group>
            <Form.Group controlId="productQuantity">
               <Form.Label>Price</Form.Label>
               <Form.Control type="number" value={productPrice}
               onChange={e => setProductPrice(e.target.value)} required/>
            </Form.Group>
         </Modal.Body>
         <Modal.Footer>
            <Button variant="secondary" onClick={closeAdd}>Close</Button>
            <Button variant="success" type="submit">Submit</Button>
         </Modal.Footer>
      </Form>
   </Modal>
   </>
   )
}

export default AdminView