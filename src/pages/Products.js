import { Fragment, useState, useEffect } from 'react';
import { Container, Row, Col } from "react-bootstrap";
import 'react-loading-skeleton/dist/skeleton.css'
import ProductCard from "../components/ProductCard";

export default function Products ({fetchData}) {

		// console.log(fetchData);  // undefined
		const [ products, setProducts ] = useState([]);
		// const [ users, setUsers ] = useState([]);
		
		// console.log(products);  // []

		useEffect(()=> {

			fetch(`${ process.env.REACT_APP_API_URL }/products/`)
			.then(res=>res.json())
			.then(data=> {
				console.log(data);
				setProducts(data.map(product=>{
					return(
						<ProductCard key={product._id} productProp={product} />	
					);
				}));
			})
		},[])

	return(	
		<>
		<h3 className="my-5 pt-5 text-center">All Products</h3>
		<Fragment>
			<Container>
				<Row>
					<Col className='catalog justify-content-center'>
							{products}
					</Col>
				</Row>
			</Container>
		</Fragment>
		</>
	)
}