import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Col, Row, FloatingLabel } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const [fullName, setFullName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	useEffect(()=>{
		if( ( fullName !== '' && mobileNo.length >= 3 && email !== '' && password1 !== '' && password2 !== '' ) && ( password1 === password2 ) ){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [ fullName, mobileNo, email, password1, password2 ]);

	function registerUser(e){
		e.preventDefault();
		fetch(`${ process.env.REACT_APP_API_URL}/users/checkEmail`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {
		   console.log(data);
		   if(data === true){
		   	Swal.fire({
		    		title: "Duplicate	email found",
		    		icon: "error",
		    		text: "Kindly provide another email address."
		    	})
		   }else{
		    	fetch(`${ process.env.REACT_APP_API_URL}/users/register`, {
		    	   method: "POST",
		    	   headers: {
		    	      'Content-Type': 'application/json'
		    	   },
		    	   body: JSON.stringify({
						fullName: fullName,
						email: email,
						mobileNo: mobileNo,
						password: password1,
		    	   })
		    	})
		    	.then(res => res.json())
		    	.then(result => {
		    	   if (result === true) {
		    	        // Clear input fields
		    	      setFullName("");
		    	      setEmail("");
		    	      setMobileNo("");
		    	      setPassword1("");
		    	      setPassword2("");
		    	      Swal.fire({
							title: 'Registration successful',
							icon: 'success',
							text: 'Welcome to Simply Clear Skin!'
		    	   	});
	    	            navigate('/login')
		    	   } else {
		    	      Swal.fire({
		    	            title: 'Something wrong',
		    	            icon: 'error',
		    	            text: 'Please try again.'   
		    	      });
		    	   }
		    	})
		   }
		})
	}

	return (
		// (user.email !== null) ?
		(user.id !== null) ?
		<Navigate to='/products' />
		:
		<Container className="mt-5-lg pt-2 my-4">
			<Row className="justify-content-center mt-5 my-5">
				<Col className="form-col2 shadow-sm p-4" md={4}>
					<Form className="py-4 px-3" onSubmit={(e) => registerUser(e)}>
						<Form.Group controlId="username">
							<h3 className="text-center pb-4">Get Started!</h3>
							<FloatingLabel
								label="Full Name"
								className="mb-3">
								<Form.Control className="form-control rounded-3 text-muted"
								type="text"
								placeholder="name@domain.com"
								value={fullName}
								onChange={e => setFullName(e.target.value)}
								required />
							</FloatingLabel>
						</Form.Group>
						<Form.Group className="pb-1" controlId="userEmail">
							{/* <Form.Label className="form-label">Password</Form.Label> */}
							<FloatingLabel
								controlId="floatingInput"
								label="Email Address"
								className="mb-1">
								<Form.Control className="form-control rounded-3 text-muted"
								type="email"
								placeholder="Enter email"
								value={email}
								onChange={e => setEmail(e.target.value)}
								required />
							</FloatingLabel>
							<Form.Text className="text-muted">&#160;&#160;We'll never share your email with anyone else.
	      				</Form.Text>
						</Form.Group>
						<Form.Group controlId="password1">
							<FloatingLabel
								controlId="floatingInput"
								label="Mobile Number"
								className="mb-3">
								<Form.Control className="form-control rounded-3 text-muted"
								type="text"
								placeholder="Enter mobile number"
								value={mobileNo}
								onChange={e => setMobileNo(e.target.value)}
								required />
							</FloatingLabel>
						</Form.Group>
						<Form.Group controlId="password1">
							<FloatingLabel
								controlId="floatingInput"
								label="Password"
								className="mb-3">
								<Form.Control className="form-control rounded-3 text-muted"
								type="password"
								placeholder="********"
								value={password1}
								onChange={e => setPassword1(e.target.value)}
								required />
							</FloatingLabel>
						</Form.Group>
						<Form.Group className="pb-1" controlId="password2">
							<FloatingLabel
								controlId="floatingInput"
								label="Verify Password"
								className="mb-3">
								<Form.Control className="form-control rounded-3 text-muted"
								type="password"
								placeholder="********"
								value={password2}
								onChange={e => setPassword2(e.target.value)}
								required />
							</FloatingLabel>
							<Form.Check className="pt-2" type="checkbox" label="By signing up, you agree to the Terms and Conditions">
							</Form.Check>
						</Form.Group>
						<Form.Group>
							<div className="d-grid gap-2 mt-3 mb-4">
								{isActive ?
									<Button type="submit" id='submitBtn'>Register</Button>
									:
									<Button type="submit" id='submitBtn' disabled>Register</Button>}
							</div>
						</Form.Group>
						<div>
							<p className="text-center mb-1">Already have an account?&#160; Click <span><a href="/register" className="form-link"> here </a></span>to login.</p>
							<p className="text-center"><a className="form-link" href="#!">Forgot password?</a></p>
						</div>
					</Form>
				</Col>
			</Row>
		</Container>
	);
}
