import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Form, Button, FloatingLabel } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from "../UserContext";
// import styled from "styled-components";
import Swal from 'sweetalert2';

export default function Login() {

	const { user, setUser } = useContext(UserContext);
	const [ email, setEmail ] = useState("");
	const [ password, setPassword ] = useState("");
	const [ isActive, setIsActive] = useState(false);

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			},
		})
		.then(res => res.json())
		.then(data=>{
			console.log(data);
			setUser({
				id: data._id,
				isAdmin: data.isAdmin,
				email: data.email
			})
		})
	}
	useEffect(()=>{
		if (email !== ''&& password !== '') {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password]);

	function loginUser(e){
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (typeof data.access !== 'undefined') {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);
				Swal.fire({
					title:"Login Successful!",
					icon: "success",
					text: "Welcome!"
				})
			} else{
				Swal.fire({
					title: "Authentication Failed.",
					icon: "error",
					text: "Check your login credentials and try again."
				})
			}
		});
		setEmail('');
		setPassword('');
	}
	return(
		(user.id !== null) ?
		<Navigate to='/' />
		:
		<Container className="mt-5-lg pt-2 my-4">
			<Row className="justify-content-center mt-5 my-5">
				<Col className="form-col2 shadow-sm p-3" md={4}>
					<Form className="py-5 px-3" onSubmit={(e) => loginUser(e)}>
						<Form.Group controlId="userEmail">
							<h3 className="text-center pb-4">Welcome Back!</h3>
							<FloatingLabel
								controlId="floatingInput_1"
								label="Email address"
								className="mb-3">
								<Form.Control className="form-control rounded-3 text-muted"
								type="email"
								placeholder="name@domain.com"
								value={email}
								onChange={e => setEmail(e.target.value)}
								required />
							</FloatingLabel>
						</Form.Group>
						<Form.Group className="pb-1" controlId="password1">
							<FloatingLabel
								controlId="floatingInput–2"
								label="Password"
								className="mb-3">
								<Form.Control className="form-control rounded-3 text-muted"
								type="password"
								placeholder="********"
								value={password}
								onChange={e => setPassword(e.target.value)}
								required />
							</FloatingLabel>
						</Form.Group>
						<Form.Group className="px-2" controlId="formBasicCheckbox">
							<Form.Check className="form-check pb-3" type="checkbox" label="Keep me logged in">
							</Form.Check>
						</Form.Group>
						<Form.Group>
							<div className="d-grid gap-2 mt-3 mb-4">
								{isActive ?
									<Button type="submit" id='submitBtn'>Log in</Button>
									:
									<Button type="submit" id='submitBtn' disabled>Log in</Button>}
							</div>
						</Form.Group>
						<div>
							<p className="text-center mb-1">Don't have an account?&#160;  Click <span><a href="/register" className="form-link">here </a></span>to register.</p>
							<p className="text-center"><a className="form-link" href="#!">Forgot password?</a></p>
						</div>
					</Form>
				</Col>
			</Row>
		</Container>
	);
}