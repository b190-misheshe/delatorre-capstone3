import { useContext, Fragment, useState } from 'react';
import { Container, Nav, Navbar, Offcanvas } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from './UserContext';
import { RiAccountPinCircleLine, RiShoppingCart2Line, RiAccountPinCircleFill  } from "react-icons/ri";
import { MdLogout } from "react-icons/md";

import logo from './images/simply-logo.png'
import Cart from "./components/Cart";

export default function AppNavbar(){

	const { user } = useContext(UserContext);

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	return(
		<>
		<Container fluid>
			<Navbar style={{ display: "flex", width: "100%"}} fixed="top" id="navbar">
				<Container> 
					<Nav className="ps-5" style={{ display: "flex", flex: "1"}} >
						<Navbar.Brand as={ Link } to='/'>
							<img src={logo} alt="" className="logo-img"/>
						</Navbar.Brand>
					</Nav>
			</Container>
			<Container className="highlightTextOut">
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav style={{ display: "flex", flex: "2", justifyContent: "center"}}>
						<Nav.Link id="basic-navbar-link" as={ NavLink } to='/' exact>Home</Nav.Link>
						<Nav.Link id="basic-navbar-link" as={ NavLink } to='/products' exact>Products</Nav.Link>
						{(user.isAdmin === true) ?
						<Nav.Link id="basic-navbar-link" as={ NavLink } to='/dashboard' exact>Dashboard</Nav.Link>
						: <></>}
					</Nav>	
				</Navbar.Collapse>
			</Container>
			<Container>
				<Nav className="me-5" style={{ display: "flex", flex: "1", justifyContent: "flex-end"}}>
					{(user.id !== null) ?
					<Fragment>
						{user.isAdmin === true ? 
						<Nav.Link className="m-0 align-items-center" as={ NavLink } to='/dashboard' exact>hello, admin!</Nav.Link>
						:
						<Nav.Link className="m-0 align-items-center">welcome, new user!</Nav.Link>}
						<Nav.Link onClick={handleShow} exact>
							<RiShoppingCart2Line className="nav-icons"/>
						</Nav.Link>
						<Nav.Link as={ NavLink } to='/logout'>
							<MdLogout className="nav-icons"/></Nav.Link>
					</Fragment>
					:
					<Fragment>
						<Nav.Link as={ NavLink } to='/login' exact>
							<RiAccountPinCircleLine className="nav-icons"/>  Login
						</Nav.Link>
						<Nav.Link as={ NavLink } to='/register' exact>
							<RiAccountPinCircleFill className="nav-icons"/> Register
						</Nav.Link>
					</Fragment>
					}
				</Nav>
			</Container>
			</Navbar>
		</Container>

			<Offcanvas show={show} onHide={handleClose} placement="end">
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Shopping Cart</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Cart />
        </Offcanvas.Body>
      </Offcanvas>
</>









	)
}
