const sliderData = [
	{
		id: "001",
		name: "Rice Toner",
		brand: "IM FROM",
		image: "https://i.imgur.com/Kvt03dR.png",
		description: "Eliminates excess oils and dead skin cells whilst giving the skin large amounts of hydration.",
		category: "Toner",
		price: 24,
		onOffer: true
	},
	{
		id: "002",
		name: "Cleansing Balm",
		brand: "CLEAN IT ZERO",
		image: "https://i.imgur.com/LAG6fOg.png",
		description: "Dissolves sebum, dirt and heavy make-up without using harsh ingredients or drying out the skin.",
		category: "Cleanser",
		price: 12,
		onOffer: true
	},
	{
		id: "003",
		name: "All Clean Balm",
		brand: "HEIMISH",
		image: "https://i.imgur.com/E0I0VcK.png",
		description: "Removes tough waterproof makeup and impurities without stripping your face of its natural oils nor does it leave behind a greasy residue.",
		category: "Cleanser",
		price: 18,
		onOffer: true
	},
	{
		id: "004",
		name: "S J Foam Cleanser",
		brand: "ETUDE HOUSE",
		image: "https://i.imgur.com/ykB8BXO.png",
		description: "Removes all the impurities on the skin with soft creamy foam, leaving the skin feeling moisturised and smooth without feeling tight.",
		category: "Cleanser",
		price: 12,
		onOffer: true
	},
	{
		id: "005",
		name: "Bio-Peel Gauze Peeling",
		brand: "NEOGEN",
		image: 'https://i.imgur.com/1UfgHG6.png',
		description: "Removes stubborn accumulated dead skin cells and pore-clogging impurities without stripping the skin - leaving the skin smooth, bright and glass-like.",
		category: "Expoliator",
		price: 22,
		onOffer: true
	},
	{
		id: "006",
		name: "AHA-BHA-PHA Miracle Toner",
		brand: "SOME BY MI",
		image: "https://i.imgur.com/jJjvpuV.png",
		description: "Removes dead skin, impurities. excess sebum and gently exfoliate the skin leaving it clear and healthy.",
		category: "Toner",
		price: 14,
		onOffer: true
	},
	{
		id: "007",
		name: "Advanced Snail Mucin Essence",
		brand: "COSRX",
		image: 'https://i.imgur.com/R00bToF.png',
		description: "Locks in moisture deep into the skin, plump the skin to improve the appearance of fine lines, and has the ability to repair and strengthen the skin barrier",
		category: "Essence",
		price: 18,
		onOffer: true
	},
	{
		id: "008",
		name: "Cream Skin Refiner",
		brand: "LANEIGE",
		image: "https://i.imgur.com/8kpfyyL.png",
		description: "Hydrates and gives your skin 12 hours of long-lasting moisture while also strengthens the skin barrier",
		category: "Essence",
		price: 22,
		onOffer: true
	},
	{
		id: "009",
		name: "Dark Spot Correcting Glow Serum",
		brand: "AXIS-Y",
		image: "https://i.imgur.com/kjWpMMz.png",
		description: "Brightens, softens and leaves the skin looking radiant that is perfect for all skin types.",
		category: "Serum",
		price: 14,
		onOffer: true
	},
	{
		id: "010",
		name: "Great Barrier Relief",
		brand: "KRAVE BEAUTY",
		image: "https://i.imgur.com/O9dzxO6.png",
		description: "Soothes, heals and strengthens your skin barrier while evening out your complexion.",
		category: "Serum",
		price: 38,
		onOffer: true
	},
	{
		id: "011",
		name: "Glow Serum",
		brand: "BEAUTY OF JOSEON",
		image: "https://i.imgur.com/B2Jne1H.png",
		description: "Soothes, fights acne and minimizes pores - giving your skin a radiant glow.",
		category: "Serum",
		price: 12,
		onOffer: true
	},
	{
		id: "012",
		name: "Jeju Cherry Blossom Jelly Cream",
		brand: "INNISFREE",
		image: "https://i.imgur.com/2FuDJTq.png",
		description: "Brightens skin tone, stimulates collagen formation, protects the skin from free radicals and treats any irritation/redness.",
		category: "Moisturiser",
		price: 14,
		onOffer: true
	},
	{
		id: "013",
		name: "Moisturizing Multi Balm Stick",
		brand: "KAHI",
		image: "https://i.imgur.com/XBMYyOf.png",
		description: "Brightens, increases the collagen synthesis in the skin to restore elasticity, gives the skin long-lasting hydration and helps minimise the appearance of pores.",
		category: "Moisturiser",
		price: 30,
		onOffer: true
	},
	{
		id: "014",
		name: "UV Aqua Rich Watery Essence",
		brand: "BIORE",
		image: "https://i.imgur.com/D8E7DT9.png",
		description: "Boasts star ingredients such as Hyaluronic Acid and Royal Jelly Extract that hydrates and provides anti-aging care, while also ensures every area of the skin is evenly covered and fully protected.",
		category: "Sunscreeen",
		price: 14,
		onOffer: true
	},
	{
		id: "015",
		name: "Artless Glow Base SPF50+",
		brand: "HEIMISH",
		image: "https://i.imgur.com/d5XonTP.png",
		description: "Tones and preps your skin for your makeup, evening out pores and texture and suitable to use on its own for that silk like look and radiant glow. ",
		category: "Sunscreen",
		price: 14,
		onOffer: true
	},
	{
		id: "016",
		name: "Moistfull Collagen Deep Cream",
		brand: "ETUDE HOUSE",
		image: "https://i.imgur.com/u5xcrk2.png",
		description: "Provides moisture and gives the skin intense anti-aging care and long-lasting hydration.",
		category: "Sunscreen",
		price: 14,
		onOffer: true
	},
]

export default sliderData;


